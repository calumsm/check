package main

import (
	"fmt"

	"bitbucket.org/calumsm/check/checkout"
)

func main() {
	c := checkout.NewCheckout()
	p := checkout.NewPricing()
	p.Totaller = checkout.CheckoutTotaller
	p.AddNewPromotion(checkout.NewPromotion("A", 3, 130))
	p.AddNewPromotion(checkout.NewPromotion("B", 2, 45))
	p.AddNewProduct(checkout.NewProduct("A", "Widget A", 50))
	p.AddNewProduct(checkout.NewProduct("B", "Widget B", 30))
	p.AddNewProduct(checkout.NewProduct("C", "Widget C", 20))
	p.AddNewProduct(checkout.NewProduct("D", "Widget D", 15))
	c.Pricing = p
	scanList := []checkout.SkuId{ //5 A's, 3 B's, 5 C's, 2 D's
		"A",
		"A",
		"B",
		"C",
		"B",
		"B",
		"C",
		"C",
		"A",
		"C",
		"A",
		"A",
		"D",
		"D",
		"C",
	}
	for _, item := range scanList {
		c.Scan(item)
	}
	fmt.Println("Totals:")
	for skuId, quantity := range c.Transactions {
		fmt.Printf("\t%v (%v) = %v\n", c.Pricing.Lines[skuId].Name, quantity,
			checkout.CalculatePriceForProducts(
				c.Pricing.Lines[skuId],
				quantity,
				c.Pricing.Promotions[skuId]))

	}
	fmt.Printf("Checkout Total: %v\n", c.GetTotalPrice())
}
