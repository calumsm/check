package checkout

type Checkout interface {
	Scan(item SkuId)

	GetTotalPrice() float64
}

type SkuId string

type MainCheckout struct {
	Pricing      *PricingSystem
	Transactions map[SkuId]int
}

func NewCheckout() *MainCheckout {
	return &MainCheckout{
		Pricing:      NewPricing(),
		Transactions: make(map[SkuId]int),
	}
}

func (m *MainCheckout) Scan(item SkuId) {
	_, ok := m.Transactions[item]
	if ok {
		m.Transactions[item] = m.Transactions[item] + 1
	} else {
		m.Transactions[item] = 1
	}
}

func (m *MainCheckout) GetTotalPrice() float64 {
	return m.Pricing.Totaller(m.Pricing.Lines, m.Transactions, m.Pricing.Promotions)
}
