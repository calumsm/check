package checkout

import (
	"testing"
)

func TestMainCheckoutIsCheckout(t *testing.T) {
	var _ Checkout = NewCheckout()
}

func TestCheckoutScan(t *testing.T) {
	checkout := NewCheckout()
	checkout.Scan("A")
	expected := 1
	if len(checkout.Transactions) != expected {
		t.Errorf("Expected Transaction list of length %v; Got %v", expected, len(checkout.Transactions))
	}
}

func TestAddTransactionForSameItems(t *testing.T) {
	checkout := NewCheckout()

	p1 := NewProduct("A", "A", 0.4)
	checkout.Pricing.AddNewProduct(p1)
	checkout.Scan("A")
	checkout.Scan("A")
	expected := 2
	if checkout.Transactions["A"] != expected {
		t.Errorf("Scanned wrong number of items, expected: %v; Got: %v ", expected, checkout.Transactions["A"])
	}
}

func TestAddTransactionForMultipleProducts(t *testing.T) {
	checkout := NewCheckout()

	p1 := NewProduct("A", "A Widget", 0.4)
	checkout.Pricing.AddNewProduct(p1)
	checkout.Scan("A")
	checkout.Scan("A")
	checkout.Scan("B")
	expected := 2
	if len(checkout.Transactions) != expected {
		t.Errorf("Scanned wrong number of items, expected: %v; Got: %v ", expected, len(checkout.Transactions))
	}
}

func TestCheckoutTotaller(t *testing.T) {
	checkout := NewCheckout()
	p1 := NewProduct("A", "A Widget", 0.4)
	p2 := NewProduct("B", "B Gadget", 1.6)
	checkout.Pricing.AddNewProduct(p1)
	checkout.Pricing.AddNewProduct(p2)
	checkout.Scan("A")
	checkout.Scan("A")
	checkout.Scan("B")
	expected := 2.4
	result := CheckoutTotaller(checkout.Pricing.Lines, checkout.Transactions, checkout.Pricing.Promotions)
	if expected != result {
		t.Errorf("Expected: %v; Got: %v", expected, result)
	}
}

func TestOverallCheckout(t *testing.T) {

	checkout := NewCheckout()
	checkout.Pricing.Totaller = CheckoutTotaller
	p1 := NewProduct("A", "A Widget", 0.4)
	p2 := NewProduct("B", "B Gadget", 1.6)
	checkout.Pricing.AddNewProduct(p1)
	checkout.Pricing.AddNewProduct(p2)
	checkout.Scan("A")
	checkout.Scan("A")
	checkout.Scan("B")
	expected := 2.4
	result := checkout.GetTotalPrice()
	if expected != result {
		t.Errorf("Expected: %v; Got: %v", expected, result)
	}
}

func TestOverallCheckoutWithPromotions(t *testing.T) {

	checkout := NewCheckout()
	checkout.Pricing.Totaller = CheckoutTotaller
	p1 := NewProduct("A", "A Widget", 0.4)
	p2 := NewProduct("B", "B Gadget", 1.6)
	p3 := NewProduct("C", "C Thing", 5)

	promo1 := NewPromotion("A", 5, 1.8)
	promo2 := NewPromotion("C", 3, 12)

	checkout.Pricing.AddNewPromotion(promo1)
	checkout.Pricing.AddNewPromotion(promo2)
	checkout.Pricing.AddNewProduct(p1)
	checkout.Pricing.AddNewProduct(p2)
	checkout.Pricing.AddNewProduct(p3)

	/*
		1.8 for 5A's
		4.8 for 3 B's
		17 for 4 C's
	*/

	scanList := []SkuId{ //5 A's, 3 B's, 4 C's
		"A",
		"A",
		"B",
		"C",
		"B",
		"B",
		"C",
		"C",
		"A",
		"C",
		"A",
		"A",
	}
	for _, item := range scanList {
		checkout.Scan(item)
	}
	expected := 23.6
	result := checkout.GetTotalPrice()
	if expected != result {
		t.Errorf("Expected: %v; Got: %v", expected, result)
	}
}
