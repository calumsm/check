package checkout

import "testing"

func TestAddProductLines(t *testing.T) {
	checkout := NewCheckout()
	p1 := NewProduct("A", "A Widget", 0.4)

	checkout.Pricing.AddNewProduct(p1)
	expected := 1
	if len(checkout.Pricing.Lines) != expected {
		t.Errorf("Expected Lines list of length %v; Got %v", expected, len(checkout.Pricing.Lines))
	}
}

func TestCheckSetPrice(t *testing.T) {
	p := NewPricing()
	p.AddNewProduct(NewProduct("A", "A", 1))
	err := p.SetPrice("A", 2)
	if err != nil {
		t.Errorf("Error raised %v", err)
	}
	if p.Lines["A"].UnitPrice != 2 {
		t.Errorf("Expected 2; Got %v", p.Lines["A"].UnitPrice)
	}
}

func TestCheckSetPriceNotExists(t *testing.T) {
	p := NewPricing()
	p.AddNewProduct(NewProduct("A", "A", 1))
	err := p.SetPrice("B", 2)
	if err == nil {
		t.Errorf("Error Should have been raised ")
	}

}

func TestCheckSetNewPromotion(t *testing.T) {
	p := NewPricing()
	p.AddNewPromotion(NewPromotion("A", 4, 20))
	p.SetNewPromotion("A", NewPromotion("A", 5, 30))

	if p.Promotions["A"].PricePerMultiple != 30 {
		t.Errorf("Expected 30; Got %v", p.Promotions["A"].PricePerMultiple)

	}
}

func TestAddProductLinesToExisting(t *testing.T) {
	checkout := NewCheckout()
	p1 := NewProduct("A", "A Widget", 0.4)
	p2 := NewProduct("B", "B Gadget", 12)
	err := checkout.Pricing.AddNewProduct(p1)
	if err != nil {
		t.Errorf("Error adding product; %v", err)
	}
	err = checkout.Pricing.AddNewProduct(p2)
	if err != nil {
		t.Errorf("Error adding product; %v", err)
	}
	expected := 2
	if len(checkout.Pricing.Lines) != expected {
		t.Errorf("Expected Lines list of length %v; Got %v", expected, len(checkout.Pricing.Lines))
	}
}

func TestAddDuplicateProduct(t *testing.T) {
	checkout := NewCheckout()
	p1 := NewProduct("A", "A Widget", 0.4)
	p2 := NewProduct("A", "A Widget", 12)
	err := checkout.Pricing.AddNewProduct(p1)
	if err != nil {
		t.Errorf("Error adding product; %v", err)
	}
	err = checkout.Pricing.AddNewProduct(p2)
	if err == nil {
		t.Errorf("A Duplicate product should throw an error; %v", err)
	}
	expected := 1
	if len(checkout.Pricing.Lines) != expected {
		t.Errorf("Expected Lines list of length %v; Got %v", expected, len(checkout.Pricing.Lines))
	}
}

func TestAddDuplicatePromotion(t *testing.T) {
	checkout := NewCheckout()
	err := checkout.Pricing.AddNewPromotion(NewPromotion("A", 1, 1))
	if err != nil {
		t.Errorf("Error adding product; %v", err)
	}
	err = checkout.Pricing.AddNewPromotion(NewPromotion("A", 1, 1))
	if err == nil {
		t.Errorf("A Duplicate Promotion should throw an error; %v", err)
	}
	expected := 1
	if len(checkout.Pricing.Promotions) != expected {
		t.Errorf("Expected Lines list of length %v; Got %v", expected, len(checkout.Pricing.Lines))
	}
}

func TestProducts(t *testing.T) {
	promo := NewPromotion("A", 4, 20)
	p1 := NewProduct("A", "A Widget", 6.0)

	result := CalculatePriceForProducts(p1, 3, promo)
	expected := float64(18)
	if result != expected {
		t.Errorf("Expected %v; Got %v", expected, result)
	}
}

func TestProductsWithPromotionAndRemainder(t *testing.T) {
	promo := NewPromotion("A", 4, 20)
	product := NewProduct("A", "A Widget", 6.0)

	result := CalculatePriceForProducts(product, 6, promo)
	expected := float64(32)
	if result != expected {
		t.Errorf("Expected %v; Got %v", expected, result)
	}
}

func TestProductsWithPromotion(t *testing.T) {
	promo := NewPromotion("A", 4, 20)
	product := NewProduct("A", "A Widget", 6.0)

	result := CalculatePriceForProducts(product, 4, promo)
	expected := float64(20)
	if result != expected {
		t.Errorf("Expected %v; Got %v", expected, result)
	}

}

func TestProductsWithMultiplePromotionOccuring(t *testing.T) {
	promo := NewPromotion("A", 4, 20)
	product := NewProduct("A", "A Widget", 6.0)

	result := CalculatePriceForProducts(product, 8, promo)
	expected := float64(40)
	if result != expected {
		t.Errorf("Expected %v; Got %v", expected, result)
	}
}

func TestProductsWithMultiplePromotionOccuringAndRemainder(t *testing.T) {
	promo := NewPromotion("A", 4, 20)
	product := NewProduct("A", "A Widget", 6.0)

	result := CalculatePriceForProducts(product, 10, promo)
	expected := float64(52)
	if result != expected {
		t.Errorf("Expected %v; Got %v", expected, result)
	}
}

func TestProductsWithNoPromotion(t *testing.T) {
	product := NewProduct("A", "A Widget", 6.0)

	result := CalculatePriceForProducts(product, 10, nil)
	expected := float64(60)
	if result != expected {
		t.Errorf("Expected %v; Got %v", expected, result)
	}
}

func TestMultipleProductsTotal(t *testing.T) {
	promo := NewPromotion("A", 4, 20)
	product := NewProduct("A", "A Widget", 6.0)
	product2 := NewProduct("B", "B Gadget", 4.0)
	result := CalculatePriceForProducts(product, 10, promo)
	result = result + CalculatePriceForProducts(product2, 5, nil)
	expected := float64(72)
	if result != expected {
		t.Errorf("Expected %v; Got %v", expected, result)
	}
}
