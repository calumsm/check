package checkout

import (
	"fmt"
	"math"
)

type PricingSystem struct {
	Lines      map[SkuId]*Product
	Promotions map[SkuId]*Promotion
	Totaller   PriceCalculator
}

type Promotion struct {
	Sku              SkuId
	Multiple         int
	PricePerMultiple float64
}

type Product struct {
	Sku       SkuId
	Name      string
	UnitPrice float64
}

type PriceCalculator func(map[SkuId]*Product, map[SkuId]int, map[SkuId]*Promotion) float64

func NewPricing() *PricingSystem {
	return &PricingSystem{make(map[SkuId]*Product), make(map[SkuId]*Promotion), nil}
}

func NewPromotion(sku SkuId, multiple int, pricePerMultiple float64) *Promotion {
	return &Promotion{sku, multiple, pricePerMultiple}
}

func NewProduct(sku SkuId, name string, unitPrice float64) *Product {
	return &Product{sku, name, unitPrice}
}

func (p *PricingSystem) AddNewProduct(prod *Product) error {
	existing, ok := p.Lines[prod.Sku]
	if ok {
		return fmt.Errorf(fmt.Sprintf("A Product with SkuId %v is already defined: %v", prod.Sku, existing.Name))
	} else {
		p.Lines[prod.Sku] = prod
	}
	return nil
}

func (p *PricingSystem) SetPrice(skuId SkuId, unitPrice float64) error {
	if existing, ok := p.Lines[skuId]; ok {
		existing.UnitPrice = unitPrice
		return nil
	} else {
		return fmt.Errorf(fmt.Sprintf("Unknown Product: %v ", skuId))
	}
}

func (p *PricingSystem) SetNewPromotion(skuId SkuId, newPromotion *Promotion) {
	p.Promotions[skuId] = newPromotion
}

func (prod *PricingSystem) AddNewPromotion(promo *Promotion) error {
	_, ok := prod.Promotions[promo.Sku]
	if ok {
		return fmt.Errorf(fmt.Sprintf("A Promotion for SkuId %v is already defined", promo.Sku))
	} else {
		prod.Promotions[promo.Sku] = promo
	}
	return nil
}

func CheckoutTotaller(products map[SkuId]*Product, items map[SkuId]int, promotions map[SkuId]*Promotion) float64 {
	runningTotal := 0.0

	for skuId, quantity := range items {
		runningTotal = runningTotal + CalculatePriceForProducts(products[skuId], quantity, promotions[skuId])
	}
	return math.Round(runningTotal*100) / 100
}

func CalculatePriceForProducts(p *Product, quantity int, promo *Promotion) float64 {

	if promo == nil {
		return p.UnitPrice * float64(quantity)
	} else {
		quantityEffectedByPromo := float64(quantity / promo.Multiple)
		promotionTotal := float64(promo.PricePerMultiple) * quantityEffectedByPromo
		nonPromoQuantity := math.Mod(float64(quantity), float64(promo.Multiple))
		nonPromoTotal := float64(p.UnitPrice) * nonPromoQuantity
		overallTotal := float64(promotionTotal) + nonPromoTotal
		return overallTotal
	}
}
